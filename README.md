# ⚠️ Deprecated
This repository is no longer maintained and has been moved to [https://github.com/zeerx7/reverse-ip](https://github.com/zeerx7/reverse-ip). Please update your references.

# Reverse IP Lookup (V 1.3)

![Screenshot](https://i.ibb.co/LgQ26kg/Screenshot.png)

Takes a domain or IP address and does a reverse lookup to quickly shows all other domains hosted from the same server. Useful for finding phishing sites or identifying other sites on the same shared hosting server.

### Requirements
> OS: Windows

### Usage
- Download rev_special.exe from this repository or click [here](https://bit.ly/reverse-ip)
- Run the executable

_`Thanks.`_
